import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';


const appRoutes: Routes = [
{path:'', loadChildren:'./home/home.module#HomeModule'},
{path:'products/:id', loadChildren:'./products/products.module#ProductsModule'},

];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes);
