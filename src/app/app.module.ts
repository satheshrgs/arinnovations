import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { ProductsModule } from './products/products.module';
import { AppRoutingModule } from './app-routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,HomeModule,AppRoutingModule,ProductsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
