import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  id:any;
  products:any;
  product=[
    {
      "productName":"Product 1",
      "productDescription":"The system helps in the collection of materials for storage, conveying of materials without any damage as well as spillage. Packing of material with accurate and required amount of weight without any wastage of materials.",
      "productImage":"/assets/img/products/1.JPG"
    },
    {
      "productName":"Product 2",
      "productDescription":"The system creates a hassle free, safe environment for operator during maintenance and plays a major role in safe handling of heavy materials with less time which increases in the productivity.",
      "productImage":"/assets/img/products/2.JPG"
    },
    {
      "productName":"Product 3",
      "productDescription":"Conversion of Real time machines, assemblies into 3D Animation, which helps in the effective understanding of operating principles, for the trainees",
      "productImage":"/assets/img/products/3.JPG"
    }
  ]
  constructor(private route: ActivatedRoute) {

  this.route.params.subscribe(res => this.id=res.id);

  this.products=this.product[this.id-1];
 }


  ngOnInit() {
  }

}
