import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { productsRoutingModule } from './products-routing';

@NgModule({
  imports: [
    CommonModule,productsRoutingModule
  ],
  declarations: [ProductsComponent]
})
export class ProductsModule { }
