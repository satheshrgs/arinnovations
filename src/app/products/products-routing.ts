import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { ProductsComponent } from './products/products.component';


const playerDetailsRoutes: Routes = [
{path:'products/:id',component:ProductsComponent},

];

export const productsRoutingModule: ModuleWithProviders = RouterModule.forChild(playerDetailsRoutes);
