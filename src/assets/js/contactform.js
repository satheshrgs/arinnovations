$(document).ready(function(){

    (function($) {
        "use strict";


    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                number: {
                    required: true,
                    minlength: 10
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 15
                }
            },
            messages: {
                name: {
                    required: "Please Enter your Name",
                    minlength: "your name must consist of at least 2 characters"
                },
                subject: {
                    required: "Please type your subject",
                    minlength: "your subject must consist of at least 4 characters"
                },
                number: {
                    required: "Please enter your contact number",
                    minlength: "your Number must consist of at 10 characters"
                },
                email: {
                    required: "no email, no message"
                },
                message: {
                    required: "You have to write something to send this form.",
                    minlength: "thats all? really?"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"https://script.google.com/macros/s/AKfycbxB0xbXru8PC2Me5XmqQUmTVIbOYMN0M7gQ30pZnQtYUzm1pBQ3/exec",
	                   dataType: "json",
                    success: function(result) {
                       $("#errormessage").fadeOut();
                        $('#contactForm :input').attr('disabled', 'disabled');

                        $('#contactForm').fadeTo( "slow", 1, function()
              						{
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
              							$('#contactSendMessage').remove();
              							$("#sendmessage").fadeIn();
                                      })
                    },
                    error: function(result) {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                           $("#errormessage").fadeIn();
                        })
                    }
                })
            }
        })
    })

 })(jQuery)
})
